# Angular Auth Essential
![alt text](https://gitlab.com/vlzdavid12/two-part-auth-angular-essential/-/raw/master/screenshot.png)

## Note data.json
Part of commit name JSON DATA - find the json to run  with npm json-server.
```
$ json-server --watch /Users/macbook/Desktop/data/db.json 
```
<hr/>

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.15.

## Command Basic Esential
```
ng version
ng serve -c dev
ng serve --configuration = local

ng g c pages/about --skipTests -is --flat —dry-run
ng g m pages/posts --flat --skipTests
ng g m appRouting  --routing --flat
ng g s service/data --skipTests=true 
ng g p pipes/data --skipTests=true 
ng g d directive/data 
```


<table>
<thead>
<tr>
<th>Command</th>
<th>Alias</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td><p><tt>ng new</tt></p></td>
<td><p>
</p></td>
<td><p>Creates a new Angular application</p></td>
</tr>
<tr>
<td><p><tt>ng serve</tt></p></td>
<td><p>
</p></td>
<td><p>Builds and runs the angular application for testing</p></td>
</tr>
<tr>
<td><p><tt>ng eject</tt></p></td>
<td><p>
</p></td>
<td><p>Makes the webpack config files available to be edited</p></td>
</tr>
<tr>
<td><p><tt>ng generate component [name]</tt></p></td>
<td><p><tt>ng g c [name]</tt></p></td>
<td><p>Creates a new component </p></td>
</tr>
<tr>
<td><p><tt>ng generate directive [name]</tt></p></td>
<td><p><tt>ng g d [name]</tt></p></td>
<td><p>Creates a new directive</p></td>
</tr>
<tr>
<td><p><tt>ng generate module [name]</tt></p></td>
<td><p><tt>ng g m [name]</tt></p></td>
<td><p>Creates a module</p></td>
</tr>
<tr>
<td><p><tt>ng generate pipe [name]</tt></p></td>
<td><p><tt>ng g p [name]</tt></p></td>
<td><p>Creates a pipe</p></td>
</tr>
<tr>
<td><p><tt>ng generate service [name]</tt></p></td>
<td><p><tt>ng g s [name]</tt></p></td>
<td><p>Creates a service</p></td>
</tr>
<tr>
<td><p><tt>ng generate enum [name]</tt></p></td>
<td><p><tt>ng g e [name]</tt></p></td>
<td><p>Creates an enumeration</p></td>
</tr>
<tr>
<td><p><tt>ng generate guard [name]</tt></p></td>
<td><p><tt>ng g g [name]</tt></p></td>
<td><p>Creates a guard</p></td>
</tr>
<tr>
<td><p><tt>ng generate interface [name]</tt></p></td>
<td><p><tt>ng g i [name]</tt></p></td>
<td><p>Creates an interface</p></td>
</tr>
</tbody>
</table>
