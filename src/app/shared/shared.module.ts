import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageErrorComponent} from './pages/page-error/page-error.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {MaterialModule} from '../material/material.module';
import {RouterModule} from '@angular/router';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
  declarations: [
    PageErrorComponent,
    SidebarComponent,
    ToolbarComponent
  ],
  exports: [
    PageErrorComponent,
    SidebarComponent,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ]
})
export class SharedModule {
}



