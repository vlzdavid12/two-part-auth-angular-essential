import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/services/auth.service';
import {User} from '../../auth/interface/interface';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styles: [`.space{
    flex: 1 1 auto
  }`]
})
export class ToolbarComponent implements OnInit {

  @Input('sideNav') sidenav!: any;

  constructor(private router: Router,
              private authService: AuthService) { }

  ngOnInit(): void {}

  get auth(): User{
    return this.authService.authUser;
  }

  logout(): void{
    this.router.navigate(['./auth']);
  }

}
