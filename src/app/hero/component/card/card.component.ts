import {Component, Input, OnInit} from '@angular/core';
import {Hero} from '../../interface/hero.interface';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styles: [
  ]
})
export class CardComponent implements OnInit {

  @Input('itemHero') item!: Hero;

  constructor() { }

  ngOnInit(): void {
  }

}
