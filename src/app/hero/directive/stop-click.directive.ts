import {Directive, HostListener} from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[click-stop-propagation]'
})
export class StopClickDirective {

  @HostListener('click', ['$event'])
  public onClick($event: Event): boolean{
    $event.preventDefault();
    $event.stopPropagation();
    return false;
  }

}
