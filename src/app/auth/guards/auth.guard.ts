import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Router, RouterStateSnapshot, UrlSegment, UrlTree, Route } from '@angular/router';
import {AuthService} from '../services/auth.service';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {

  constructor(private authService: AuthService,
              private router: Router) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.authService.verifyAuth().pipe(tap(auth => {
      if (!auth){
        this.router.navigate(['/auth/login']);
      }
    }));
/*    if (this.authService.authUser.id){
      return true;
    }
    return false;*/
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | boolean  {
    return this.authService.verifyAuth().pipe(tap(auth => {
      if (!auth){
        this.router.navigate(['/auth/login']);
      }
    }));
/*    if (this.authService.authUser.id){
      return true;
    }
    return false;*/
  }
}
