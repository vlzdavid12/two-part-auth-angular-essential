import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../interface/interface';
import {Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _url: string =  environment.urlAPI;
  private _auth: User | undefined;

  constructor(private http: HttpClient) { }


  get authUser(): User{
    // tslint:disable-next-line:no-non-null-assertion
    return {...this._auth!};
  }

  verifyAuth(): Observable<boolean> {
    if (!localStorage.getItem('token')){
      return of(false);
    }
    return this.http.get<User>(`${this._url}/usuarios/1`)
      .pipe(map(auth => {
        this._auth = auth;
        return true;
      }) );
  }

  login(): Observable<User>{
    return this.http.get<User>(`${this._url}/usuarios/1`)
      .pipe(tap((auth: User) => this._auth =  auth),
        tap((auth: User) => localStorage.setItem('id', String(auth.id)))
      );
  }


}
